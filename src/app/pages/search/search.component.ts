import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../../services/search.service';
import { LocationService } from '../../services/location.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  @Output() setMarkers: EventEmitter<object> = new EventEmitter<object>();
  @Output() setMapCenter: EventEmitter<object> = new EventEmitter<object>();
  lat: number = 0;
  lng: number = 0;
  scoutLocation = '';

  selectedItems = [];

  constructor(private searchService: SearchService,
              private router: Router,
              private locationService: LocationService) { }

   ngOnInit() {
     this.lat = 0;
     this.lng = 0;
   }

   openCandidates() {

     let url = `/browse-candidate`;
     let technologies = this.selectedItems.map((item) => {
       return item;
     });

     let start = true;
     if ( this.lng ) {
       url+= `?lng=${this.lng}`;
       start = false;
     }
     if ( this.lat ) {
       if (start) {
         url+= `?lat=${this.lat}`;
       } else {
         url+= `&lat=${this.lat}`;
       }
     }
     if ( technologies.length) {
       if (start) {
         url+= `?technologies=${technologies}`;
       } else {
         url+= `&technologies=${technologies}`;
       }
     }
     this.router.navigateByUrl(url);
   }

   getGeoLocation() {
     console.log("setting geo location");
     if(window.navigator.geolocation){
       window.navigator.geolocation.getCurrentPosition(this.setPosition.bind(this));
     };
   }

   setPosition(position) {
     this.lat = position.coords.latitude;
     this.lng = position.coords.longitude;
     this.locationService.getAddressByLatLng(this.lat, this.lng).subscribe(
       res => {
         console.log(res);
         this.scoutLocation = res.results[0].formatted_address;
       },
       error => {
         console.log(error);
       }
     );

     this.setMapCenter.emit({lat: position.coords.latitude, lng: position.coords.longitude})
     //this.scoutLocation = `latitude: ${position.coords.latitude}, longitude: ${position.coords.longitude}`;
   }
   updateTechnologies(items:any){
         console.log(items);
         console.log(this.selectedItems);
         this.selectedItems = items;
   }

   updateLocation(location: any) {
     console.log(location);
     this.lat = location.lat;
     this.lng = location.lng;
     this.scoutLocation = location.scoutLocation;
   }


}
