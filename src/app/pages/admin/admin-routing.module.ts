import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const AdminRoutes: Routes = [
    { path: 'admin/dashboard', component: DashboardComponent },
  ];

@NgModule({
  imports: [ RouterModule.forRoot(AdminRoutes) ],
  exports: [ RouterModule ]
})

export class AdminRoutingModule {}
