import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AsideModule } from 'ng2-aside';
import { AdminRoutingModule } from "./admin-routing.module";

import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    FormsModule,
    AdminRoutingModule,
    CommonModule,
    AsideModule
  ]
})
export class AdminModule { }
