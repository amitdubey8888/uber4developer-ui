import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { UserService } from './../../../services/user.service';
import { FreelancerService } from './../../../services/freelancer.service';
import { ClientService } from './../../../services/client.service';
import { NotificationService } from './../../../services/notification.service';
import { JobService } from './../../../services/job.service';
import { Freelancer } from '../../../shared/models/freelancer.model';
import { Client } from '../../../shared/models/client.model';
import { SignUp } from '../../../shared/models/signup.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  dashboard:boolean=true;
  n_freelancers:boolean=false;
  n_clients:boolean=false;
  freelancers:boolean=false;
  clients:boolean=false;
  add_freelancer_client:boolean=false;
  add_freelancer:boolean=false;
  update_freelancer:boolean=false;
  add_client:boolean=false;
  update_client:boolean=false;
  notification_details:boolean=false;
  freelance:Freelancer={};
  signup:SignUp={};
  client_model:Client={};
  freelancers_notifications = [];
  clients_notifications = [];
  total_freelancer:[{
    id:'',
    linkedin_url: '',
    github_url: '',
    resume: '',
    location: {
      lat: 0,
      lng: 0
    },
    rating: 0,
    experience_in_years: 0,
    description: '',
    user:{
      first_name: '',
      last_name: '',
      phone: 0,
      email: ''
    }
  }];
  total_client = [{
    company: '',
    location: {
      lat: 0,
      lng: 0
    },
    rating: 0,
    userId: '',
    user:{
      first_name: '',
      last_name: '',
      phone: 0,
      email: ''
    }
  }];
  freelancer_count:Number=0;
  client_count:Number=0;
  token:any;
  get_job = {
    title: '',
    description: '',
    clientId: '',
    status: '',
    domain: '',
    technologies: '',
    job_location: {
      lat: 0,
      lng: 0
    },
    job_type: '',
    categories: ''
  };
  get_freelancer = {
    experience_in_years:'',
    location: {
      lat: 0,
      lng: 0
    },
    rating: 0,
    user:{
      first_name: '',
      last_name: '',
      phone: 0,
      email: ''
    }
  };
  get_client = {
    company:'',
    location: {
      lat: 0,
      lng: 0
    },
    rating: 0,
    user:{
      first_name: '',
      last_name: '',
      phone: 0,
      email: ''
    }
  };
  notification_data = {
    id:'',
    sentBy: '',
    status: '',
    message: '',
    notes: '',
    clientId: '',
    jobId: '',
    freelancerId: ''
  };

  constructor(private toasterService: ToasterService, 
              private route: Router, 
              private userService: UserService,
              private freelancerService: FreelancerService,
              private clientService: ClientService,
              private notificationService: NotificationService,
              private jobService: JobService) { }

  ngOnInit() {
    this.token = localStorage.getItem('admin_token');
    if(this.token==null){
      this.toasterService.pop("Error", 'You are not admin!');
      this.route.navigateByUrl('/admin');
    }
    this.getNotifications();
    this.getFreelancers();
    this.getClients();
  }

  f_dashboard(){
    this.dashboard=true;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer_client=false;
    this.notification_details=false;        
  }

  f_n_freelancers(){
    this.dashboard=false;
    this.n_freelancers=true;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer_client=false;
    this.notification_details=false;    
  }

  f_n_clients(){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=true;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer_client=false;
    this.notification_details=false;    
  }

  f_freelaners(){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=true;
    this.clients=false;
    this.add_freelancer_client=false;
    this.notification_details=false;    
  }

  f_clients(){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=true;
    this.add_freelancer_client=false;
    this.notification_details=false;    
  }

  freelancer(){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer_client=true;
    this.add_freelancer=true;
    this.add_client=false; 
    this.notification_details=false;
    this.add_freelancer=true;
    this.update_freelancer=false;
    this.add_client=false;
    this.update_client=false;       
  }

  client(){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer_client=true;
    this.add_client=true;  
    this.add_freelancer=false;
    this.notification_details=false;
    this.add_freelancer=false;
    this.update_freelancer=false;
    this.add_client=true;
    this.update_client=false;
  }

  getNotifications(){
    this.notificationService.GetAllNotification().subscribe(
      res => {
        let f_count_value=0;
        let c_count_value=0;
        res.forEach(element => {
          if(element.sentBy=='freelancer'){
            if(element.status=='unresolved'){
              f_count_value++;
            }
            this.freelancers_notifications.push(element);
          }
          if(element.sentBy=='client'){
            if(element.status=='unresolved'){
              c_count_value++;
            }
            this.clients_notifications.push(element);
          }
        });
        this.freelancer_count = f_count_value;
        this.client_count = c_count_value;
        this.freelancers_notifications = (this.freelancers_notifications).reverse();
        this.clients_notifications = (this.clients_notifications).reverse();
      },
      error => {
        console.log(error);
      }
    );
  }

  getNotification(data){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer_client=false;
    this.add_client=false;  
    this.add_freelancer=false;
    this.notification_details=true;
    this.notification_data=data;

    //Get Job
    this.jobService.GetAdminJob(data.jobId, this.token).subscribe(
      res => {
        this.get_job = res;
        this.get_job.domain = ((res.domain).toString()).replace(/[^a-zA-Z0-9\.,]/g, ' ');
        // this.get_job.categories = ((res.categories).toString()).replace(/[^a-zA-Z0-9\.,]/g, ' ');
        this.get_job.technologies = ((res.technologies).toString()).replace(/[^a-zA-Z0-9\.,]/g, ' ');
      },
      error => {
        console.log(error);
      }
    );

    //Get Freelancer
    this.freelancerService.GetAdminFreelancers(data.freelancerId, this.token).subscribe(
      res => {
        this.get_freelancer = res;
      },
      error => {
        console.log(error);
      }
    );

    //Get Client
    this.clientService.GetAdminClient(data.clientId, this.token).subscribe(
      res => {
        this.get_client = res;
      },
      error => {
        console.log(error);
      }
    );

  }

  updateNotification(){
    this.notification_data.status='resolved';
    this.notificationService.UpdateAdminNotification(this.notification_data.id, this.notification_data, this.token).subscribe(
      res => {
        this.toasterService.pop('success', 'Notification updated Successfully !');
        this.freelancers_notifications = [];
        this.clients_notifications = [];
        this.getNotifications();
      },
      error => {
        console.log(error);
      }
    );
  }

  deleteNotification(data){
    this.notificationService.DeleteNotification(data.id).subscribe(
      res => {
        this.toasterService.pop('success', 'Notification deleted Successfully !');
        this.freelancers_notifications = [];
        this.clients_notifications = [];
        this.getNotifications();
      },
      error => {
        console.log(error);
      }
    );
  }

  getFreelancers() {
    this.freelancerService.GetAdminAllFreelancers(this.token).subscribe(
     res => {
       this.total_freelancer = res;
     },
     error => {
       console.log(error);
     }
    );
  }

  addFreelancer(){
    
    this.userService.RegisterUser(this.signup).subscribe(
      res => {
        let new_freelance = {
          location: {
            lat:0,
            lng:0
          },
          technologies: [],
          description: this.freelance.description,
          linkedin_url: this.freelance.linkedin_url,
          github_url: this.freelance.github_url,
          rating: this.freelance.rating,
          experience_in_years: this.freelance.experience_in_years,
          date_of_birth: this.freelance.date_of_birth,
          resume: '',
          userId:res.id
        };
        this.freelancerService.AddAdminFreelancer(new_freelance).subscribe(
          response=>{
            this.toasterService.pop("success",'Freelancer Added Successfully !');
            this.total_freelancer=null;
            this.getFreelancers();
          },
          error =>{
            this.toasterService.pop("Error", error.statusText);
          }
        );
      },
      error => {
        console.log(error);
        this.toasterService.pop("Error",error.statusText);
        this.toasterService.pop("Error",'Please fill all fields correctly !');
      }
    );
  }

  updateFreelancerForm(value){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer=false;
    this.notification_details=false;
    this.add_freelancer=false;
    this.update_freelancer=true;
    this.add_client=false;
    this.update_client=false;
    this.add_freelancer_client=true;
    this.freelance = value;
    this.signup = value.user;
  }

  updateFreelancer(){
    this.freelancerService.UpdateAdminFreelancer(this.freelance.id, this.freelance, this.token).subscribe(
      res => {
        this.userService.updateAdminUser(this.signup.id, this.signup, this.token).subscribe(
          res => {
            this.toasterService.pop('success', 'Freelancer updated Successfully !');
            this.getFreelancers();
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }

  removeFreelancer(freelance){
    this.freelancerService.RemoveFreelancer(freelance.id, this.token).subscribe(
      res => {
        this.toasterService.pop('success', 'Freelancers deleted Successfully !');
        this.getFreelancers();
      },
      error => {
        console.log(error);
      }
    );
  }

  getClients(){
    this.clientService.GetAdminAllClient(this.token).subscribe(
      res => {
        this.total_client = res;
      },
      error => {
        console.log(error);
      }
    );
  }
  
  addClient(){
    this.userService.RegisterUser(this.signup).subscribe(
      res => {
        this.client_model.userId = res.id;
        this.clientService.AddAdminClient(this.client_model).subscribe(
          response=>{
            this.toasterService.pop("success",'Client Added Successfully !');
            this.total_client= null;
            this.getClients();
          },
          error =>{
            this.toasterService.pop("Error", error.statusText);
          }
        );
      },
      error => {
        console.log(error);
        this.toasterService.pop("Error",error.statusText);
        this.toasterService.pop("Error",'Please fill all fields correctly !');
      }
    );
  }

  updateClientForm(value){
    this.dashboard=false;
    this.n_freelancers=false;
    this.n_clients=false;
    this.freelancers=false;
    this.clients=false;
    this.add_freelancer=false;
    this.notification_details=false;
    this.add_freelancer=false;
    this.update_freelancer=false;
    this.add_client=false;
    this.update_client=true;
    this.add_freelancer_client=true;
    this.client_model = value;
    this.signup = value.user;
  }

  updateClient(){
    console.log(this.client_model);
    console.log(this.signup);
    this.clientService.UpdateAdminClient(this.client_model.id, this.client_model, this.token).subscribe(
      res => {
        this.userService.updateAdminUser(this.signup.id, this.signup, this.token).subscribe(
          res => {
            this.toasterService.pop('success', 'Client updated Successfully !');
            this.getClients();
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log(error);
      }
    );
  }
  
  removeClient(value){
    this.clientService.RemoveClient(value.id, this.token).subscribe(
      res => {
        this.toasterService.pop('success', 'Client deleted Successfully !');
        this.getClients();
      },
      error => {
        console.log(error);
      }
    );
  }

  logout(){
    this.userService.LogoutUser(this.token).subscribe(
      res => {
        localStorage.clear();
        this.toasterService.pop('success', 'LoggedOut Successfully !');
        this.route.navigateByUrl('/admin');
      },
      error => {
        console.log(error);
      }
    );
  }

}
