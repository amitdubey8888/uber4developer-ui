import { Component } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})

export class AdminComponent {
  username: string;
  password: string;
  constructor(private toasterService: ToasterService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  login(){
    if(this.username==undefined || this.password==undefined){
      this.toasterService.pop('error', 'Please enter your username & password !');
      return false;      
    }
    else
    {
      this.userService.LoginUser(this.username, this.password).subscribe(
      res => {
        localStorage.setItem('user',JSON.stringify(res));
        localStorage.setItem('admin_token', res.id);
        this.toasterService.pop('success', 'Loggedin Successfully !');
        this.router.navigateByUrl('/admin/dashboard');
      },
      error => {
        console.log(error);
        this.toasterService.pop('error', error.statusText);
        }
      );
    }
  }
}
