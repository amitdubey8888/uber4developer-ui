import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FreelancerService } from '../../services/freelancer.service';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-select-technologies',
  templateUrl: './select-technologies.component.html',
  styleUrls: ['./select-technologies.component.scss']
})

export class SelectTechnologiesComponent implements OnInit {
  dropdownList = [];
  dropdownSettings = {};
  @Input() selectedItems: Array<any>  = [];
  @Input() technologiesName: string;


  @Output() updateTechnologies: EventEmitter<any> = new EventEmitter<any>();


  constructor(private freelancerService: FreelancerService,
              private toasterService: ToasterService) { }

  ngOnInit() {
    this.getTechnologies();
    //this.getFreelancers();
    this.dropdownSettings = {
          singleSelection: false,
          text:"Choose Technologies",
          selectAllText:'Select All',
          unSelectAllText:'UnSelect All',
          enableSearchFilter: true,
          classes:"select-technologies"
        };
  }

  getTechnologies() {
    this.freelancerService.getTechnologies().subscribe(
     res => {
       let list  = res.map((item) => item.name);
       list = list.filter((it, i) => {
         return list.indexOf(it) === i;
       })
       this.dropdownList = list.map((item, index) => {
         return { "value": item, "label": item };
       });
     },
     error => {
       console.log(error);
       this.toasterService.pop("error", "Unable to get data, please try again!");
     }
   )
  }

  updateSelectedItems(items:any){
        console.log(this.selectedItems);
        this.selectedItems = items;
        this.updateTechnologies.emit(this.selectedItems)
  }

}
