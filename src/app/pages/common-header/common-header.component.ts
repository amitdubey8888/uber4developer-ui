import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-common-header',
  templateUrl: './common-header.component.html',
  styleUrls: ['./common-header.component.scss']
})
export class CommonHeaderComponent implements OnInit {
  isLoggedIn:boolean=false;
  userRole: string = '';
  activeElement: string = '';

  constructor(private toasterService: ToasterService,
              private router: Router,
              private userService: UserService) { }

  ngOnInit() {
  	if(localStorage.getItem('email') != null || localStorage.getItem('id')!=null){
      this.isLoggedIn=true;
      if (localStorage.getItem('userRole') === '') {
        this.userService.getUserDetails().subscribe(
          res => {
            this.userRole = res.role;
            localStorage.setItem('userRole', res.role);
          },
          error => {
            this.toasterService.pop("error","Unable to get user information, Please try again!");
          }
        )
      } else {
        this.userRole = localStorage.getItem('userRole');
      }

    }
    this.activeElement = window.location.pathname.split('/')[1];
  }

  logout(){
    console.log(localStorage.getItem('id'));
    let id = localStorage.getItem('id');
    this.userService.LogoutUser(id).subscribe(
     res => {
       // console.log('Response ',res);
     },
     error => {
       // console.log(error);
     }
   );
  this.isLoggedIn=false;
  localStorage.clear();
  this.toasterService.pop("success","Logged Out Successfully !");
  this.router.navigateByUrl('/signin');
  }

}
