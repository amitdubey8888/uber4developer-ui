import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { JobService } from '../../services/job.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-manage-job',
  templateUrl: './manage-job.component.html',
  styleUrls: ['./manage-job.component.scss']
})
export class ManageJobComponent implements OnInit {

  userId:string;
  jobs:[{}];

  constructor(public common: CommonService, private toasterService: ToasterService, private router: Router, private jobService: JobService) { }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.getjob();
  }

  getjob(){
    this.jobService.GetJob(this.userId).subscribe(
     res => {
       this.jobs = res;
     },
     error => {
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }

  preview(job:any){
    localStorage.setItem('editJob', JSON.stringify(job));
    this.router.navigate(['job-preview']);
  }

  edit(job:any){
    localStorage.setItem('count','1');
    localStorage.setItem('editJob', JSON.stringify(job));
  	this.router.navigate(['edit-job']);
  }

  markedit(){
  	console.log('markedit');
  }

  delete(id){
    this.jobService.DeleteJob(id).subscribe(
     res => {
       this.getjob();
       this.toasterService.pop('success','Job Deleted Successfully !');
     },
     error => {
       console.log(error);
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }

}

