import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, NgZone, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-location-autocomplete',
  templateUrl: './location-autocomplete.component.html',
  styleUrls: []
})

export class LocationAutocompleteComponent implements OnInit {

  public searchControl: FormControl;

  @Input() scoutLocation: string;
  @Output() updateLocation: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone) { }

  ngOnInit() {
    this.searchControl = new FormControl();
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
            //get the place result
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
            this.updateLocation.emit({'lat': place.geometry.location.lat(), 'lng': place.geometry.location.lng(), 'scoutLocation': place.formatted_address});
          });
        });
    });
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.scoutLocation && changes.scoutLocation.previousValue !== changes.scoutLocation.currentValue) {
      this.searchElementRef.nativeElement.value = changes.scoutLocation.currentValue;
    }
    // You can also use categoryId.previousValue and
    // categoryId.firstChange for comparing old and new values

  }

}
