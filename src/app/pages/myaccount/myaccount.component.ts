import { Component, OnInit, NgZone, ViewChild, Input } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { FreelancerService } from '../../services/freelancer.service';
import { NgForm } from '@angular/forms';
import { AgmMap } from '@agm/core';
import { DatePipe } from '@angular/common';
import { Freelancer } from '../../shared/models/freelancer.model';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.scss']
})

export class MyaccountComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  lat: number;
  lng: number;
  @ViewChild(AgmMap) public agmMap: AgmMap;
  @Input('markerClickable') clickable: boolean = true;

  freelancer = { technologies: new Array<string>(),
                 location : {},
                 date_of_birth: '',
                 linkedin_url: '',
                 github_url: '',
                 experience_in_years: 0,
                 description: '',
                 resume: '' };
  actualValues: Freelancer= {};
  constructor(private freelancerService: FreelancerService,
              private toasterService: ToasterService,
              private zone:NgZone,
              public datepipe: DatePipe,
              public router: Router) { }

  ngOnInit() {
    //this.getTechnologies();
    this.getFreelancers();

        this.dropdownSettings = {
                                  singleSelection: false,
                                  text:"Choose Technologies",
                                  selectAllText:'Select All',
                                  unSelectAllText:'UnSelect All',
                                  enableSearchFilter: true,
                                  classes:"myclass custom-class selected-list .c-list .c-token"
                                };

  }

  onItemSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
    }
    onSelectAll(items: any){
        console.log(items);
    }
    onDeSelectAll(items: any){
        console.log(items);
    }

  markerMove(event){
    (<HTMLInputElement>document.getElementById('location')).value = (event.coords.lat).toFixed(2)+','+(event.coords.lng).toFixed(2);
  }

  getTechnologies() {
    this.freelancerService.getTechnologies().subscribe(
     res => {
       this.dropdownList = res.map((item, index) => {
         return { "id": index, "itemName": item.name };
       });
       console.log('myaccountt',this.dropdownList)
     },
     error => {
       console.log(error);
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }

  getFreelancers() {
    let userId = localStorage.getItem('userId');
    if ( userId && userId !== '') {
      this.freelancerService.GetFreelancerById("5a689072af7ec000148b1531").subscribe(
       res => {

         this.zone.run(() => {
                   this.actualValues = {};
                   this.freelancer = res;
                   this.selectedItems = this.freelancer.technologies;
                   this.lat = res.location.lat;
                   this.lng = res.location.lng;
                    this.freelancer.location  = res.location.lat + ', ' + res.location.lng;
                    let dob = new Date(res.date_of_birth);
                    this.freelancer.date_of_birth = `${dob.getMonth() + 1}/${dob.getDate()}/${dob.getFullYear()}`;
                    Object.assign(this.actualValues, res);
                 });
       },
       error => {
         console.log(error);
         this.toasterService.pop("Error",error.statusText);
       }
     )
    } else {
      this.toasterService.pop("warning", "login and try again!")
      this.router.navigateByUrl('/signin')
    }
  }

  updateFreelancer() {
    let e  = (<HTMLSelectElement>document.getElementById("technologies")).options;
    let dob = (<HTMLInputElement>document.getElementById("datepicker")).value;
    let update: Freelancer = {};

    update.linkedin_url = this.freelancer.linkedin_url;
    update.github_url = this.freelancer.github_url;
    update.experience_in_years = this.freelancer.experience_in_years;
    update.description = this.freelancer.description;
    update.resume = this.freelancer.resume;
    update.date_of_birth = new Date(dob);

    // if (this.actualValues.linkedin_url !== this.freelancer.linkedin_url) {
    //   update.linkedin_url = this.freelancer.linkedin_url;
    // }
    // if (this.actualValues.github_url !== this.freelancer.github_url) {
    //   update.github_url = this.freelancer.github_url;
    // }
    // if (this.actualValues.experience_in_years !== this.freelancer.experience_in_years) {
    //   update.experience_in_years = this.freelancer.experience_in_years;
    // }
    // if (this.actualValues.description !== this.freelancer.description) {
    //   update.description = this.freelancer.description;
    // }
    // if (this.actualValues.resume !== this.freelancer.resume) {
    //   update.resume = this.freelancer.resume;
    // }
    // if (this.actualValues.date_of_birth !== dob) {
    //   update.date_of_birth = new Date(dob);
    // }
    update.location = this.freelancer.location;

    //dropdown -- technologies
    let flag = false;
    for(let i=0; i< e.length; i++) {
      if (e[i].selected && this.freelancer.technologies.indexOf(e[i].innerHTML) === -1) {
        this.freelancer.technologies.push(e[i].innerHTML);
        flag = true;
      } else if(!e[i].selected && this.freelancer.technologies.indexOf(e[i].innerHTML) !== -1) {
        this.freelancer.technologies.splice(this.freelancer.technologies.indexOf(e[i].innerHTML), 1);
        flag = true;
      }
    }

    update.technologies = this.freelancer.technologies;
    // if (flag) {
    //   update.technologies = this.freelancer.technologies;
    // }
    if (Object.keys(update).length) {
      this.freelancerService.UpdateFreelancer("5a689072af7ec000148b1531", update).subscribe(
        res => {
          this.toasterService.pop("success", "Updated Successfully")
        },
        error => {
          console.log(error);
          this.toasterService.pop("Error",error.statusText);
        }
      )
    }

    console.log('updating', update);
  }

  openmap(){
    this.agmMap.triggerResize();
  }

  fileChange(event) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0 ) {
        let file: File = fileList[0];
        let formData: FormData = new FormData();
        console.log("FILE",file) ;
        formData.append('file', file, file.name);
        this.freelancerService.uploadResume(formData).subscribe(
         res => {
           this.freelancer.resume = res.result.s3url;
           console.log("Successfully uploaded")
         },
         error => {
           console.log(error);
           this.toasterService.pop("Error",error.statusText);
         }
       )
    }
}

}
