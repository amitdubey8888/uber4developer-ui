import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { JobService } from '../../services/job.service';
import { Job } from '../../shared/models/job.model';

@Component({
  selector: 'app-browse-job',
  templateUrl: './browse-job.component.html',
  styleUrls: ['./browse-job.component.scss']
})
export class BrowseJobComponent implements OnInit {

  jobs = [
          { title:String,
            job_location:{
              lat:String,
              lng:String
            },
            description:String,
            domain:String
          }
         ];

  constructor(
              private toasterService: ToasterService,
              private router: Router,
              private jobService: JobService) { }

  ngOnInit() {
    this.getallJob();
  }

  getallJob(){
    this.jobService.GetAllJob().subscribe(
     res => {
       this.jobs = res;
       localStorage.setItem('updateJob',JSON.stringify(this.jobs[0]));
     },
     error => {
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }
  getme(value){
    if(typeof value === "string"){
      return value.replace(/[^a-zA-Z0-9\.,]/g, ' ');
    }else{
      var result = "" ;
      value.forEach(function(x){
          result +=  x.replace(/[^a-zA-Z0-9\.,]/g, ' ')+" ";
      })
      return result ;
    }
  }

  openjobDetails(job){
    localStorage.setItem('jobDetail',JSON.stringify(job));
    this.router.navigate(['/job-detail']);
  }
}
