import { Component, OnInit } from '@angular/core';
import { marker } from '../shared/models/marker.model';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  isLoggedIn:boolean=false;

  constructor() { }

  ngOnInit() {
    localStorage.setItem('count','1');

  }

}
