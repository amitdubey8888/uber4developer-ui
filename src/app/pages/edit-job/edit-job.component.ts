import { Component, OnInit, Input, ViewChild, EventEmitter } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { AgmMap } from '@agm/core';
import { NgForm } from '@angular/forms';
import { Job } from '../../shared/models/job.model';
import { JobService } from '../../services/job.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-edit-job',
  templateUrl: './edit-job.component.html',
  styleUrls: ['./edit-job.component.scss']
})
export class EditJobComponent implements OnInit {
  lat: number;
  lng: number;
  @ViewChild(AgmMap) public agmMap: AgmMap;
  @Input('markerClickable') clickable: boolean = true;

  add_job = {} as Job;
  userId:string;
  mode:string;
  closeResult: string;
  job_location:string;
  
  job:{
    title:string,
    job_location:{lat:string,lng:string},
    job_type:string,
    closing_date:string,
    domain:string,
    technologies:string,
    description:string,
    id:string
  };
  
  constructor(public common: CommonService, 
              private toasterService: ToasterService, 
              private route: ActivatedRoute, 
              private router: Router, 
              private jobService: JobService) { }

  ngOnInit() {
    if(localStorage.getItem('userId')==''){
      this.router.navigate(['signin']);
    }
    if(localStorage.getItem('count')=='1'){
      localStorage.setItem('count','2');
      window.location.reload();
    } 
    this.job = JSON.parse(localStorage.getItem('editJob'));
    if(this.job) {
      this.add_job.title = this.job.title;
      this.add_job.job_location = ((this.job).job_location.lat) +' , '+ ((this.job).job_location.lng);
      this.add_job.description = this.job.description;
    }
    else{
      this.router.navigate(['manage-job']);
    }
    this.userId = localStorage.getItem('userId');
  }

  openmap(){
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.agmMap.triggerResize();
        },
        error => {
          this.toasterService.pop('error','Unable to fetch current location!');   
        }
      );
    }
  }
  
  markerMove(event){
    (<HTMLInputElement>document.getElementById('job_location')).value = (event.coords.lat).toFixed(2)+','+(event.coords.lng).toFixed(2);
  }

  update(data:Job){
    let location = (<HTMLInputElement>document.getElementById('job_location')).value;
    let job_type = (<HTMLSelectElement>document.getElementById('job_type')).value;
    let domain = document.getElementById('domain') as HTMLSelectElement;
    var domains = [];
    for(var i = 1; i < domain.options.length; i++)
    {
      if(domain.options[i].selected == true)
      {
        domains.push((domain.options[i].value).split(': ')[1]);
      }
    }

    let technology = document.getElementById('technologies') as HTMLSelectElement;
    var technologies = [];
    for(var i = 1; i < technology.options.length; i++)
    {
      if(technology.options[i].selected == true)
      {
        technologies.push((technology.options[i].value).split(': ')[1]);
      }
    }

    let job = {
      title: data.title,
      description: data.description,
      domain: domains,
      technologies: technologies,
      created_by: this.userId,
      updated_by: this.userId,
      created_at: new Date(),
      updated_at: new Date(),
      job_location: {
        lat: (location.split(','))[0],
        lng: (location.split(','))[1]
      },
      id:this.job.id
    }
    console.log(job);
    this.jobService.UpdateJob(job).subscribe(
     res => {
       localStorage.removeItem('editJob');
       this.toasterService.pop('success','Job Updated Successfully !');
       this.router.navigateByUrl('/manage-job');
     },
     error => {
       console.log(error);
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }
  goback(){
    this.router.navigate(['/manage-job']);
  }
}
