import { Component, OnInit, Input, ViewChild, EventEmitter } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { AgmMap } from '@agm/core';
import { NgForm } from '@angular/forms';
import { Job } from '../../shared/models/job.model';
import { JobService } from '../../services/job.service';
import { CommonService } from '../../services/common.service';


@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.scss']
})
export class AddJobComponent implements OnInit {
  lat: number;
  lng: number;
  @ViewChild(AgmMap) public agmMap: AgmMap;
  @Input('markerClickable') clickable: boolean = true;

  add_job = {} as Job;
  userId:string;
  mode:string;
  closeResult: string;
  job_location:string;
  constructor(public common: CommonService, 
              private toasterService: ToasterService, 
              private route: ActivatedRoute, 
              private router: Router, 
              private jobService: JobService) {
               }

  ngOnInit() {
    if(localStorage.getItem('userId')==''){
      this.router.navigate(['signin']);
    }
    this.userId = localStorage.getItem('userId');
    if(localStorage.getItem('count')=='1'){
      localStorage.setItem('count','2');
      window.location.reload();
    } 
  }
  
  openmap(){
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.agmMap.triggerResize();
        },
        error => {
          this.toasterService.pop('error','Unable to fetch current location!');   
        }
      );
    }
  }
  
  markerMove(event){
    (<HTMLInputElement>document.getElementById('job_location')).value = (event.coords.lat).toFixed(2)+','+(event.coords.lng).toFixed(2);
  }

  addjob(data:Job){
    let location = (<HTMLInputElement>document.getElementById('job_location')).value;
    let job_type = (<HTMLSelectElement>document.getElementById('job_type')).value;
    let domain = document.getElementById('domain') as HTMLSelectElement;
    var domains = [];
    for(var i = 1; i < domain.options.length; i++)
    {
      if(domain.options[i].selected == true)
      {
        domains.push((domain.options[i].value).split(': ')[1]);
      }
    }

    let technology = document.getElementById('technologies') as HTMLSelectElement;
    var technologies = [];
    for(var i = 1; i < technology.options.length; i++)
    {
      if(technology.options[i].selected == true)
      {
        technologies.push((technology.options[i].value).split(': ')[1]);
      }
    }

    let job = {
      title: data.title,
      description: data.description,
      domain: domains,
      technologies: technologies,
      created_by: this.userId,
      updated_by: this.userId,
      created_at: new Date(),
      updated_at: new Date(),
      job_location: {
        lat: (location.split(','))[0],
        lng: (location.split(','))[1]
      }
    }
    this.jobService.AddJob(job).subscribe(
     res => {
       this.toasterService.pop('success','Job Added Successfully !');
       localStorage.setItem('count','1');
       this.router.navigateByUrl('/manage-job');
     },
     error => {
       console.log(error);
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }

  goback(){
    this.router.navigate(['manage-job']);
  }
}
