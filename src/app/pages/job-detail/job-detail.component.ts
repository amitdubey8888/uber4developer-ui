import { Component, OnInit, Input, ViewChild, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AgmMap } from '@agm/core';
import { ToasterService } from 'angular2-toaster';
import { UserService } from '../../services/user.service';
import { AdminService } from '../../services/admin.service';
import { FreelancerService } from '../../services/freelancer.service';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss']
})
export class JobDetailComponent implements OnInit {

  @ViewChild(AgmMap) public agmMap: AgmMap;
  @Input('markerClickable') clickable: boolean = true;

  title:string='';
  job_location:string='';
  job_type:string='';
  domain:string='';
  technologies:string='';
  description:string='';
  closing_date:string='';
  lat:number;
  lng:number;

  email: string;
  phone: number;
  message: string;
  username: string;

  job:{
    title:string,
    job_location:{lat:string,lng:string},
    job_type:string,
    closing_date:string,
    domain:string,
    technologies:string,
    description:string
  };
  user = {};
  constructor(private router: Router,
              public toasterService: ToasterService,
              private userService: UserService,
              private adminService: AdminService,
              private freelancerService: FreelancerService) { }

  ngOnInit() {
    if(localStorage.getItem('userId')!=''){
      this.username = `${localStorage.getItem('firstname')}, ${localStorage.getItem('lastname')}`;
      this.email = localStorage.getItem('email');
      this.phone = parseInt(localStorage.getItem('phone'));
      this.message = localStorage.getItem('message');


      this.job = JSON.parse(localStorage.getItem('jobDetail'));
      if(this.job) {
        this.title = this.job.title;
        this.job_location = ((this.job).job_location.lat) +' , '+ ((this.job).job_location.lng);
        this.job_type = 'Null';
        this.closing_date = 'Null';
        this.domain = this.getme(this.job.domain);
        this.technologies = ((this.job.technologies).toString()).replace(/[^a-zA-Z0-9\.,]/g, ' ');
        this.description = this.job.description;
        this.lat = parseFloat((this.job).job_location.lat);
        this.lng = parseFloat((this.job).job_location.lng);
      }else{
        this.router.navigate(['browse-job']);
      }
    }
  }

  getme(value){
    if(typeof value === "string"){
      return value.replace(/[^a-zA-Z0-9\.,]/g, ' ');
    }else{
      var result = "" ;
      value.forEach(function(x){
          result +=  x.replace(/[^a-zA-Z0-9\.,]/g, ' ')+" ";
      })
      return result ;
    }
  }

  applyJob() {
    console.log('applying for job');
    let request: any = {};
    request.sentBy = this.username;
    request.message = this.message;
    request.freelancerId = localStorage.getItem('userId');
    request.status = 'unresolved';
    request.type = 'apply-job';
    request.jobId = JSON.parse(localStorage.jobDetail).id;


    if (!this.phone || this.email === '') {
      this.toasterService.pop('warning',"Enter all mandatory fields, i.e, contact, email")
    } else {
      this.adminService.sendNotification(request).subscribe(
        res => {
          this.toasterService.pop('success', 'Request sent successfully!');
          if (this.phone !== parseInt(localStorage.getItem('phone')) || this.email !== localStorage.getItem('email')) {
            let update: any = { 'phone': this.phone, 'email': this.email };
            this.freelancerService.UpdateFreelancer("5a689072af7ec000148b1531", update).subscribe(
              res => {
                this.toasterService.pop("success", "Contact Info Updated Successfully")
                document.getElementById('small-dialog').hidden = true;
                this.router.navigateByUrl('/browse-job');
              },
              error => {
                console.log(error);
                this.toasterService.pop("Error",error.statusText);
              }
            )
          } else {
            document.getElementById('small-dialog').hidden = true;
            this.router.navigateByUrl('/browse-job');
          }
        },
        error => {
          this.toasterService.pop('Error', 'Something went wrong, Please try again!');
        }
      )
    }
  }
}
