import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FreelancerService } from '../../services/freelancer.service';
import { SearchService } from '../../services/search.service';

type marker = {
  lat: number,
  lng: number,
  label: string,
  draggable: boolean,
  technologies: [string]
}

@Component({
  selector: 'app-browse-candidate',
  templateUrl: './browse-candidate.component.html',
  styleUrls: ['./browse-candidate.component.scss']
})
export class BrowseCandidateComponent implements OnInit {
  freelancers = [];
  markers = [];
  lat: number = 28.589140;
  lng: number = 77.301883;

  scoutLocation = '';
  technologies = [];


  constructor(private toasterService: ToasterService,
              private router: Router,
              private freelancerService: FreelancerService,
              private searchService: SearchService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    let params = this.activatedRoute.snapshot.queryParams;
    if (Object.keys(params).length) {
      this.lat = parseFloat(params.lat);
      this.lng = parseFloat(params.lng);
      this.technologies = (params.technologies && params.technologies.split(",")) || [];
      this.getFreelancers();
      //this.getFreelancersByLocation();
    } else {
      this.getGeoLocation();
    }

  }

  getFreelancers() {
    let filters: any = {};
    filters.location = {};
    filters.location.lat = this.lat;
    filters.location.lng = this.lng;
    filters.technologies = this.technologies;

    this.freelancerService.GetFreelancers(filters).subscribe(
     res => {
       this.freelancers = res;
       console.log(res);

       let markers = res.map((item, index) => {
         return {
           lat: item.location.lat,
           lng: item.location.lng,
           label: `${index}`,
           draggable: false,
           technologies: item.technologies
         }
       })
       this.setMarkers(markers);
     },
     error => {
       console.log(error);
       this.toasterService.pop("error", "Unable to get data, please try again!");
     }
   )
  }

  getFreelancersByLocation() {
    this.searchService.getFreelancers(this.lat, this.lng).subscribe(
     res => {
       let markers = res.results.map((item, index) => {
         return {
           lat: item.location.lat,
           lng: item.location.lng,
           label: `${index}`,
           draggable: false,
           technologies: item.technologies
         }
       })
       this.setMarkers(markers);
       //console.log(res);
     },
     error => {
       console.log(error);

     }
   )
  }

  getGeoLocation() {
    console.log("setting geo location");
    if(window.navigator.geolocation){
      window.navigator.geolocation.getCurrentPosition(this.setPosition.bind(this));
    };
  }

  setPosition(position) {
    this.lat = position.coords.latitude;
    this.lng = position.coords.longitude;
    this.getFreelancers();
    //this.getFreelancersByLocation();
    //this.scoutLocation = `latitude: ${position.coords.latitude}, longitude: ${position.coords.longitude}`;
  }
  setMarkers(data) {
    console.log(data);
    this.markers = data;
  }

  getAddress(add) {
    console.log('auto address', add);
    console.log('lat - auto', add.geometry.location.lat())
  }

  updateLocation(location: any) {
    console.log(location);
    this.lat = location.lat;
    this.lng = location.lng;
    this.scoutLocation = location.scoutLocation;
    this.getFreelancers();
    //this.getFreelancersByLocation();
  }

}
