import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';

import { FreelancerService } from '../../../services/freelancer.service';
import { AdminService } from '../../../services/admin.service';
import { LocationService } from '../../../services/location.service';

@Component({
  selector: 'view-candidate-page',
  templateUrl: './view-candidate.component.html',
  styleUrls: ['./view-candidate.component.scss']
})

export class ViewCandidateComponent implements OnInit {

  id: string;
  private sub: any;
  private address: string;
  private freelancer: any = {};
  lat: number;
  lng: number;
  fullname: string;
  email: string;
  freelancerId: string;

  clientname: string;
  clientphone: number;
  clientemail: string;
  message: string;

  private fragment: string;

  constructor(private toasterService: ToasterService,
              private route: ActivatedRoute,
              private freelancerService: FreelancerService,
              private locationService: LocationService,
              private router: Router,
              private adminService: AdminService ) {}

  ngOnInit() {
    this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
    let userId = localStorage.getItem('userId');
    if ( userId && userId !== '') {
      this.clientname = `${localStorage.getItem('firstname')}, ${localStorage.getItem('lastname')}`;
      this.clientphone = parseInt(localStorage.phone);
      this.clientemail = localStorage.email;
    } else {
      this.toasterService.pop("warning", "login and try again!")
      this.router.navigateByUrl('/signin')
    }
    this.sub = this.route.params.subscribe(params => {
       this.id = params['id']; // (+) converts string 'id' to a number
       this.getFreelancerbyId()
       // In a real app: dispatch action to load the details here.
    });

  }

  ngAfterViewInit(): void {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) { }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getFreelancerbyId() {
    this.freelancerService.GetFreelancerById(this.id).subscribe(
     res => {
       this.freelancer = res;
       let dob = new Date(res.date_of_birth);
       this.freelancer.date_of_birth = `${dob.getMonth() + 1}/${dob.getDate()}/${dob.getFullYear()}`;
       if (res && res.user) {
         this.fullname = `${res.user.first_name}, ${res.user.last_name}`;
         this.email = res.user.email;
         this.freelancerId = res.user.id;
       }
       this.getAddressByCoordinates(res.location.lat, res.location.lng);
     },
     error => {
       console.log(error);
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }

  getAddressByCoordinates(lat, lng) {
    this.locationService.getAddressByLatLng(lat, lng).subscribe(
      res => {
        this.address = res.results[0].formatted_address;
      },
      error => {
        console.log(error);
        this.toasterService.pop("Error",error.statusText);
      }
    );
  }

  contactFreelancer() {
    console.log('applying for job');
    let request: any = {};
    request.sentBy = this.clientname;
    request.message = this.message;
    request.freelancerId = this.freelancerId;
    request.status = 'unresolved';
    request.type = 'contact-freelancer';
    request.clientId = localStorage.getItem('userId');

    if (!this.clientphone || this.clientemail === '') {
      this.toasterService.pop('warning',"Enter all mandatory fields, i.e, contact, email")
    } else {
      this.adminService.sendNotification(request).subscribe(
        res => {
          this.toasterService.pop('success', 'Request sent successfully!');
          if (this.clientphone !== parseInt(localStorage.getItem('phone')) || this.clientemail !== localStorage.getItem('email')) {
            let update: any = { 'phone': this.clientphone, 'email': this.clientemail };
            this.freelancerService.UpdateFreelancer("5a689072af7ec000148b1531", update).subscribe(
              res => {
                this.toasterService.pop("success", "Contact Info Updated Successfully")
                document.getElementById('small-dialog').hidden = true;
                this.router.navigateByUrl('/browse-candidate');
              },
              error => {
                console.log(error);
                this.toasterService.pop("Error",error.statusText);
              }
            )
          } else {
            document.getElementById('small-dialog').hidden = true;
            this.router.navigateByUrl('/browse-candidate');
          }
        },
        error => {
          this.toasterService.pop('Error', 'Something went wrong, Please try again!');
        }
      )
    }
  }

  openModal() {
    this.router.navigate( ['/browse-candidate', this.id], {fragment: 'small-dailog'});
  }

}
