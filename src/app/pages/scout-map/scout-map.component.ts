import { Component, OnInit, Input } from '@angular/core';
import { marker } from '../../shared/models/marker.model';

@Component({
  selector: 'app-scout-map',
  templateUrl: './scout-map.component.html',
  styleUrls: ['./scout-map.component.scss']
})
export class ScoutMapComponent implements OnInit {
  @Input() markers: marker[];
  zoom: number = 10;
  @Input() lat: number;
  @Input() lng: number;

  userCoordinates = {};

  constructor() { }

  ngOnInit() {

  }

  onMouseOver(infoWindow, gm) {

        if (gm.lastOpen != null) {
            gm.lastOpen.close();
        }

        gm.lastOpen = infoWindow;

        infoWindow.open();
    }
}
