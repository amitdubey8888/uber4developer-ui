import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoutMapComponent } from './scout-map.component';

describe('ScoutMapComponent', () => {
  let component: ScoutMapComponent;
  let fixture: ComponentFixture<ScoutMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoutMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoutMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
