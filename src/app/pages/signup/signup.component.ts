import { Component, OnInit } from '@angular/core';
import { NgClass } from '@angular/common';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';

import { SignUp } from '../../shared/models/signup.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  invalid_username: boolean = false;
  invalid_firstname: boolean = false;
  invalid_lastname:boolean=false;
  invalid_phone:boolean=false;
  invalid_email:boolean=false;
  invalid_password1:boolean=false;
  invalid_password2:boolean=false;
  
  constructor(private toasterService: ToasterService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  registerUser() {
    let validation: SignupValidation = this.validateSignup();
    if (validation.valid) {
      this.registerUserSave(validation.signup);
    }

  }

  validateSignup() {
    let valid = false;
    const emailRegexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    const password1 = (<HTMLInputElement>document.getElementById('signup-password1')).value;
    const password2 = (<HTMLInputElement>document.getElementById('signup-password2')).value;
    const username = (<HTMLInputElement>document.getElementById('signup-username')).value;
    const first_name = (<HTMLInputElement>document.getElementById('signup-firstname')).value;
    const last_name = (<HTMLInputElement>document.getElementById('signup-lastname')).value;
    const phone = (<HTMLInputElement>document.getElementById('signup-phonenumber')).value;
    const email = (<HTMLInputElement>document.getElementById('signup-email')).value;
    const role = (<HTMLInputElement>document.getElementById('signup-purpose')).value;
    if(first_name){
      this.invalid_firstname=false;
    }
    if(last_name){
      this.invalid_lastname=false;
    }
    if(username){
      this.invalid_username=false;
    }
    if(phone){
      this.invalid_phone=false;
    }
    if(email){
      this.invalid_email=false;
    }
    if(password1){
      this.invalid_password1=false;
    }
    if(password2){
      this.invalid_password2=false;
    }
    if(!first_name) {
      this.toasterService.pop("error","Please enter enter your first name");
      this.invalid_firstname=true;
      let signup: SignUp;
      return { valid, signup }
    } 
    else if(!last_name) {
      this.toasterService.pop("error","Please enter your last name");
      this.invalid_lastname=true;
      let signup: SignUp;
      return { valid, signup }
    } 
    else if(!username) {
      this.toasterService.pop("error","Please enter your username");
      this.invalid_username=true;
      let signup: SignUp;
      return { valid, signup }
    } 
    else if(!phone || phone.length<10 || phone.length>10) {
      this.toasterService.pop("error","Please enter a valid phone number");
      this.invalid_phone=true;
      let signup: SignUp;
      return { valid, signup }
    } 
    else if(!email || (email.length <= 5 || !emailRegexp.test(email))) {
      this.toasterService.pop("error","Please enter a valid email");
      this.invalid_email=true;
      let signup: SignUp;
      return { valid, signup }
    } 
    else if(!password1) {
      this.toasterService.pop("error","Please enter your password");
      this.invalid_password1=true;
      let signup: SignUp;
      return { valid, signup }
    }
    else if(!password2) {
      this.toasterService.pop("error","Please re-enter your password");
      this.invalid_password2=true;
      let signup: SignUp;
      return { valid, signup }
    } 
    else if(password1!==password2) 
    {
      this.toasterService.pop("error","Your password doesn't match");
      let signup: SignUp;
      return { valid, signup };
    }  
    else{
      let signup: SignUp = {
        username,
        first_name,
        last_name,
        phone,
        email,
        role,
        password : password1,
        emailVerified : true
      }
      valid = true;
      return { valid, signup }
    }
  }
  registerUserSave(signup) {
    this.userService.RegisterUser(signup).subscribe(
     res => {
       this.toasterService.pop("success","Registered Successfully, Please Login !");
       this.router.navigateByUrl('/signin');
     },
     error => {
       console.log(error);
       this.toasterService.pop("Error",error.statusText);
     }
   )
  }

}

interface SignupValidation {
  signup: SignUp;
  valid: boolean;
}
