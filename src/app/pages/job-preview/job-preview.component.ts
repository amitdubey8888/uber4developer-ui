import { Component, OnInit, Input, ViewChild, EventEmitter } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { AgmMap } from '@agm/core';
import { NgForm } from '@angular/forms';
import { Job } from '../../shared/models/job.model';
import { JobService } from '../../services/job.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-job-preview',
  templateUrl: './job-preview.component.html',
  styleUrls: ['./job-preview.component.scss']
})
export class JobPreviewComponent implements OnInit {

  title:string='';
  job_location:string='';
  job_type:string='';
  domain:string='';
  technologies:string='';
  description:string='';
  closing_date:string='';

  job:{
    title:string,
    job_location:{lat:string,lng:string},
    job_type:string,
    closing_date:string,
    domain:string,
    technologies:string,
    description:string
  };
  
  constructor(public common: CommonService, 
              private toasterService: ToasterService, 
              private route: ActivatedRoute, 
              private router: Router, 
              private jobService: JobService) { }

  ngOnInit() {
    if(localStorage.getItem('userId')==''){
      this.router.navigate(['signin']);
    }
    this.job = JSON.parse(localStorage.getItem('editJob'));
    if(this.job) {
      this.title = this.job.title;
      this.job_location = ((this.job).job_location.lat) +' , '+ ((this.job).job_location.lng);
      this.job_type = 'Null';
      this.closing_date = 'Null';
      this.domain = (this.job.domain).replace(/[^a-zA-Z0-9\.,]/g, ' ');
      this.technologies = ((this.job.technologies).toString()).replace(/[^a-zA-Z0-9\.,]/g, ' ');
      this.description = this.job.description;
    }else{
      this.router.navigate(['manage-job']);
    }
  }
  
  goback(){
    this.router.navigate(['manage-job']);
  }
  edit(){
    localStorage.setItem('count','1');
    this.router.navigate(['edit-job']);
  }
}
