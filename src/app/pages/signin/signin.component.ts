import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  username:string;
  password:string;

  constructor(private toasterService: ToasterService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  login(){
    this.userService.LoginUser(this.username, this.password).subscribe(
     res => {
       console.log(res);
       localStorage.setItem('userId', res.userId);
       localStorage.setItem('id', res.id);
       localStorage.setItem('ttl', res.ttl);
       this.toasterService.pop("success","Loggedin Successfully !");
       this.userService.getUserDetails().subscribe(
         res => {
           localStorage.setItem('userRole', res.role);
           localStorage.setItem('username', res.username);
           localStorage.setItem('phone', res.phone);
           localStorage.setItem('email', res.email);
           localStorage.setItem('firstname', res.first_name);
           localStorage.setItem('lastname', res.last_name);
           this.router.navigateByUrl('/');
         },
         error => {
           this.toasterService.pop("error","unable to get user information, try refreshing the page");
         }
       )

     },
     error => {
       console.log(error);
       this.toasterService.pop('Error', error.statusText);
     }
   );
  }

}
