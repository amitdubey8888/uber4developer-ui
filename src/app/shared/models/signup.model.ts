export interface SignUp {
  username?: string;
  first_name?: string;
  last_name?: string;
  emailVerified?: boolean;
  email?: string;
  phone?: string;
  role?: string;
  password?: string;
  id?: string;
}
