class Location {
  lat?: number;
  lng?: number;
}

export class Freelancer{
  id?: string;
  location?: Location;
  technologies?: string[];
  description?: string;
  linkedin_url?: string;
  github_url?: string;
  rating?: number;
  experience_in_years?: number;
  date_of_birth?: Date;
  resume?: string;
  userId?:string;
}
