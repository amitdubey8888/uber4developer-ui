export interface Job {
  title: string;
  job_location: {};
  job_type: string;
  domain: string;
  technologies: string[];
  description: string;
  closing_date: string;
  created_by?: string;
  updated_by?:string;
}

