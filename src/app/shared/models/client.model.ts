export class Client{
    company?: string;
    location?: {
      lat?:0,
      lng?:0
    };
    rating?: 0;
    id?: string;
    userId?: string;
  }
  