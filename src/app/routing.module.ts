import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './pages/homepage.component';
import { AdminComponent } from './pages/admin/admin.component';
import { SignupComponent } from './pages/signup/signup.component';
import { SigninComponent } from './pages/signin/signin.component';
import { MyaccountComponent } from './pages/myaccount/myaccount.component';
import { AddJobComponent } from './pages/add-job/add-job.component';
import { ManageJobComponent } from './pages/manage-job/manage-job.component';
import { BrowseCandidateComponent } from './pages/browse-candidate/browse-candidate.component';
import { BrowseJobComponent } from './pages/browse-job/browse-job.component';
import { EditJobComponent } from './pages/edit-job/edit-job.component';
import { JobPreviewComponent } from './pages/job-preview/job-preview.component';
import { JobDetailComponent } from './pages/job-detail/job-detail.component';
import { ViewCandidateComponent } from './pages/browse-candidate/view-candidate/view-candidate.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'myaccount', component: MyaccountComponent },
  { path: 'add-job', component: AddJobComponent },
  { path: 'manage-job', component: ManageJobComponent },
  { path: 'browse-job', component: BrowseJobComponent },
  { path: 'edit-job', component: EditJobComponent },
  { path: 'job-preview', component: JobPreviewComponent },
  { path: 'job-detail', component: JobDetailComponent },
  { path: 'browse-candidate', component: BrowseCandidateComponent },
  { path: 'browse-candidate/:id', component: ViewCandidateComponent },

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class RoutingModule {}
