import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


import { Freelancer } from '../shared/models/freelancer.model';


type Freelancers = {
  results: Freelancer[]
}

@Injectable()
export class SearchService {

  constructor(private http: HttpClient) { }


  getFreelancers(lat: number, lng: number): Observable<Freelancers> {
    return this.http.get<Freelancers>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/details?lat=${lat}&lng=${lng}`);
  }

}
