import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class NotificationService {

  constructor(private http: HttpClient) { }

  GetNotification(id): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/AdminNotifications/${id}`);
  }

  GetAllNotification(): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/AdminNotifications/`);
  }

  UpdateNotification(id, update): Observable<any> {
    return this.http.put<any>(`https://radiant-lake-19220.herokuapp.com/api/AdminNotifications/${id}`, update);
  }
  
  DeleteNotification(id): Observable<any> {
    return this.http.delete<any>(`https://radiant-lake-19220.herokuapp.com/api/AdminNotifications/${id}`);
  }
  
  UpdateAdminNotification(id, update, token): Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/AdminNotifications/update?where=%7B%22id%22%3A%20%22${id}%22%7D&access_token=${token}`, update);
  }
}
