import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Job } from '../shared/models/job.model';

@Injectable()
export class JobService {

  constructor(private http: HttpClient) { }

  AddJob(addjob): Observable<Job> {
    return this.http.post<Job>(`https://radiant-lake-19220.herokuapp.com/api/Jobs`, addjob);
  }

  GetJob(id): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Jobs?filter[where][created_by]=`+id);
  }

  UpdateJob(updatejob): Observable<any> {
    return this.http.patch<any>(`https://radiant-lake-19220.herokuapp.com/api/Jobs`, updatejob);
  }

  DeleteJob(id): Observable<any> {
    return this.http.delete<any>(`https://radiant-lake-19220.herokuapp.com/api/Jobs/`+id);
  }

  GetAllJob(): Observable<any> {
    return this.http.get<any>('https://radiant-lake-19220.herokuapp.com/api/Jobs');
  }

  GetAdminJob(id, token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Jobs/findOne?filter=%7B%22id%22%3A%20%22${id}%22%7D&access_token=${token}`);
  }
}
