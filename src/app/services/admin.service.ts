import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AdminService {

  constructor(private http: HttpClient) { }

  sendNotification(data): Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/AdminNotifications`, data);
  }

}
