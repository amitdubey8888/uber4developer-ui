import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Freelancer } from '../shared/models/freelancer.model';

@Injectable()
export class LocationService {

  constructor(private http: HttpClient) { }

  getAddressByLatLng(lat, lng): Observable<any> {
    return this.http.get<any>(`http://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&sensor=true`);
  }

}
