import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { SignUp } from '../shared/models/signup.model';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  RegisterUser(signup: SignUp): Observable<SignUp> {
    return this.http.post<SignUp>(`https://radiant-lake-19220.herokuapp.com/api/AppUsers`, signup);
  }
  LoginUser(username:string, password:string): Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/AppUsers/login`, {username:username, password:password});
  }

  LogoutUser(token): Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/AppUsers/logout?access_token=${token}`,{});
  }


  getUser(id, token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/AppUsers/findOne?filter=${id}&access_token=${token}`);
  }

  getUserDetails(): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/AppUsers/${localStorage.userId}?access_token=${localStorage.id}`);
  }
  getAllUser(token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/AppUsers?access_token=${token}`);
  }

  updateAdminUser(id, update, token):Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/AppUsers/update?where=%7B%22id%22%3A%20%22${id}%22%7D&access_token=${token}`, update);
  }

}
