import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt-session';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Freelancer } from '../shared/models/freelancer.model';

@Injectable()
export class FreelancerService {

  constructor(private http: HttpClient,
              public authHttp: AuthHttp) { }

  GetFreelancerById(id): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/${id}?filter={\"include\": \"user\"}`);
  }

  GetAdminFreelancers(id, token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/findOne?filter=%7B%22include%22%3A%22user%22%2C%20%22id%22%3A%22${id}%22%7D&access_token=${token}`);
  }

  getTechnologies(): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Technologies/`);
  }


  GetAllFreelancers(): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/`);
  }
  
  GetFreelancers(filters): Observable<any> {
    let url: string = `https://radiant-lake-19220.herokuapp.com/api/Freelancers?filter=`;

    if (filters.technologies.length) {
      let technologiesList = '';
      filters.technologies.map((item, index) => {
        technologiesList = (index === 0) ? `\"${item}\"` : `${technologiesList},\"${item}\"`;
      })
      url+= `{\"where\":{\"and\": \[ {\"location\":{\"near\":\"${parseFloat(filters.location.lat)},${parseFloat(filters.location.lng)}\"}}, {\"technologies\":{"inq": [${technologiesList}]} } ]}}`;
    } else {
      url+= `\{\"where\":\{\"location\":\{\"near\":\"${parseFloat(filters.location.lat)},${parseFloat(filters.location.lng)}\"}}}`;
    }
    return this.http.get<any>(url);
  }

  UpdateFreelancer(id, update): Observable<any> {
    return this.http.put<any>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/${id}`, update);
  }

  uploadResume(formData): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
  //  headers.append('Accept', 'application/json');
    //let options = new RequestOptions({ headers: headers });
    /** No need to include Content-Type in Angular 4 */

    return this.http.post(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/uploadResume`, formData, {headers: headers});
  }

  AddAdminFreelancer(freelancer: Freelancer): Observable<Freelancer> {
    return this.http.post<Freelancer>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers`, freelancer);
  }

  GetAdminAllFreelancers(token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers?filter=%7B%22include%22%3A%22user%22%7D&access_token=${token}`);
  }

  UpdateAdminFreelancer(id, update, token): Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/update?where=%7B%22id%22%3A%20%22${id}%22%7D&access_token=${token}`, update);
  }

  
  RemoveFreelancer(id, token): Observable<any> {
    return this.http.delete<any>(`https://radiant-lake-19220.herokuapp.com/api/Freelancers/${id}?access_token=${token}`);
  }
}
