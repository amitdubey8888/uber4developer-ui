import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Client } from './../shared/models/client.model';

@Injectable()
export class ClientService {

  constructor(private http: HttpClient) { }

  GetClient(id, token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Clients/findOne?filter=${id}&access_token=${token}`);
  }

  GetAllClients(token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Clients?access_token=${token}`);
  }

  AddAdminClient(client: Client): Observable<Client> {
    return this.http.post<Client>(`https://radiant-lake-19220.herokuapp.com/api/Clients`, client);
  }

  GetAdminClient(id, token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Clients/findOne?filter=%7B%22include%22%3A%22user%22%2C%22id%22%3A%20%22${id}%22%7D&access_token=${token}`);
  }

  UpdateAdminClient(id, update, token): Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/Clients/update?where=%7B%22id%22%3A%20%22${id}%22%7D&access_token=${token}`, update);
  }

  GetAdminAllClient(token): Observable<any> {
    return this.http.get<any>(`https://radiant-lake-19220.herokuapp.com/api/Clients?filter=%7B%22include%22%3A%22user%22%7D&access_token=${token}`);
  }
  
  UpdateClient(id, token, update): Observable<any> {
    return this.http.post<any>(`https://radiant-lake-19220.herokuapp.com/api/Clients/update?where=${id}&access_token=${token}`, update);
  }

  RemoveClient(id, token): Observable<any> {
    return this.http.delete<any>(`https://radiant-lake-19220.herokuapp.com/api/Clients/${id}?access_token=${token}`);
  }
}
