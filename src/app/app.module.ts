import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadersCssModule } from 'angular2-loaders-css';
import { DatePipe } from '@angular/common'

import { RoutingModule } from './routing.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {AuthModule} from './auth.module';
import { AdminModule } from './pages/admin/admin.module';
import { AppComponent } from './app.component';
import {SelectModule} from 'angular2-select';
import {ModalModule} from "ng2-modal";

import { CommonService } from './services/common.service';
import { SearchService } from './services/search.service';
import { UserService } from './services/user.service';
import { JobService } from './services/job.service';
import { FreelancerService } from './services/freelancer.service';
import { ClientService } from './services/client.service';
import { LocationService } from './services/location.service';
import { AdminService } from './services/admin.service';
import { NotificationService } from './services/notification.service';

import { HomepageComponent } from './pages/homepage.component';
import { AdminComponent } from './pages/admin/admin.component';
import { HeaderComponent } from './pages/header/header.component';
import { CommonHeaderComponent } from './pages/common-header/common-header.component';
import { SearchComponent } from './pages/search/search.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { FooterComponent } from './pages/footer/footer.component';
import { ScoutMapComponent } from './pages/scout-map/scout-map.component';
import { SignupComponent } from './pages/signup/signup.component';
import { MyaccountComponent } from './pages/myaccount/myaccount.component';
import { SigninComponent } from './pages/signin/signin.component';
import { AddJobComponent } from './pages/add-job/add-job.component';
import { ManageJobComponent } from './pages/manage-job/manage-job.component';
import { BrowseCandidateComponent } from './pages/browse-candidate/browse-candidate.component';
import { BrowseJobComponent } from './pages/browse-job/browse-job.component';
import { EditJobComponent } from './pages/edit-job/edit-job.component';
import { JobPreviewComponent } from './pages/job-preview/job-preview.component';
import { JobDetailComponent } from './pages/job-detail/job-detail.component';
import { SelectTechnologiesComponent } from './pages/select-technologies/select-technologies.component';
import { LocationAutocompleteComponent } from './pages/location-autocomplete/location-autocomplete.component';
import { ViewCandidateComponent } from './pages/browse-candidate/view-candidate/view-candidate.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    HomepageComponent,
    MyaccountComponent,
    HeaderComponent,
    CommonHeaderComponent,
    SearchComponent,
    CategoriesComponent,
    FooterComponent,
    ScoutMapComponent,
    SignupComponent,
    SigninComponent,
    AddJobComponent,
    ManageJobComponent,
    BrowseCandidateComponent,
    BrowseJobComponent,
    EditJobComponent,
    JobPreviewComponent,
    JobDetailComponent,
    SelectTechnologiesComponent,
    LocationAutocompleteComponent,
    ViewCandidateComponent
  ],
  imports: [
    AdminModule,
    LoadersCssModule,
    RoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ToasterModule,
    HttpModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    SelectModule,
    AuthModule,
    AngularMultiSelectModule,
    MultiSelectModule,
    ModalModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8jK8I-HTL7_1ysHN52km6OLJ227O3mBo',
      libraries: ['places']
    })
  ],
  providers: [CommonService,
              DatePipe,
              SearchService,
              ToasterService,
              LocationService,
              UserService,
              JobService,
              FreelancerService,
              AdminService,
              NotificationService,
              ClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
